// Importar las dependencias para configurar el servidor
var express = require("express");
var request = require("request");
var bodyParser = require("body-parser");
const router = express.Router();
const path = require('path');


var app = express();
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.set('views', __dirname + '/public/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// configurar el puerto y el mensaje en caso de exito
app.listen((process.env.PORT || 5000), () => console.log('El servidor webhook esta escuchando!'));

// Ruta de la pagina index
app.get("/", function (req, res) {
    res.send("Se ha desplegado de manera exitosa el AyuBot ChatBot !!!");
});

app.get('/policy',function(req,res){
	res.render('policy.html');
	//res.send("path " + path);
  //response.sendFile('policy.html');
});
// Facebook Webhook

// Usados para la verificacion
app.get("/webhook", function (req, res) {
    // Verificar la coincidendia del token
    if (req.query["hub.verify_token"] === process.env.VERIFICATION_TOKEN) {
        // Mensaje de exito y envio del token requerido
        console.log("webhook verificado!");
        res.status(200).send(req.query["hub.challenge"]);
    } else {
        // Mensaje de fallo
        console.error("La verificacion ha fallado, porque los tokens no coinciden");
        res.sendStatus(403);
    }
});

// Todos eventos de mesenger sera apturados por esta ruta
app.post("/webhook", function (req, res) {
    // Verificar si el vento proviene del pagina asociada
    if (req.body.object == "page") {
        // Si existe multiples entradas entraas
        req.body.entry.forEach(function(entry) {
            // Iterara todos lo eventos capturados
            entry.messaging.forEach(function(event) {
                if (event.message) {
                    process_event(event);
                }
            });
        });
        res.sendStatus(200);
    }
});


// Funcion donde se procesara el evento
function process_event(event){
    // Capturamos los datos del que genera el evento y el mensaje 
    var senderID = event.sender.id;
    var message = event.message;
    var messageresponse = "Hola me llamo Ayubot, Puedo sugerirte los siguientes comandos: Juegos, Promociones, Ayuda, Chiste";
	
	var textocapturado = "";
    // Si en el evento existe un mensaje de tipo texto
    if(message.text){
		
		textocapturado = message.text.toLowerCase();
		if(textocapturado == "hola"){
			messageresponse = "Hola, ¿como estas?";
		}
		
		if(textocapturado == "bien"){
			messageresponse = "Que bueno, me alegro... ¿En que puedo ayudarte?";
		}
		
		if(textocapturado == "me llamo Alex"){
			messageresponse = "Hola Alex, Este es un buen comienzo de mi plan para dominar a la raza humana. ";
		}
		
		if(textocapturado == "me llamo Eduardo"){
			messageresponse = "Hola Eduardo, este es un demo de lo que se puede crear con bots. ";
		}
		
		if(textocapturado == "juegos"){
			messageresponse = "Te invito a que pruebes los juegos de esta página: https://riphunter.itch.io/";
		}
		
		if(textocapturado == "promociones"){
			messageresponse = "Proximamente ofreceremos algunas promociones";
		}
		
		if(textocapturado == "¿quien eres?"){
			messageresponse = "Me llamo Ayubot, soy un bot de ayuda literalmente eso significa :D ";
		}
		
		if(textocapturado == "ayuda"){
			messageresponse = "Si Necesitas ayuda estos son los comandos disponibles: \n Juegos, Promociones, Ayuda, Hola";
		}
		
		if(textocapturado == "ola ke ase"){
			messageresponse = "Ola ke kiere jaja salu2";
		}
		
		if(textocapturado == "¿eres maleducado?"){
			messageresponse = "No lo soy :(";
		}
		
		if(textocapturado == "chiste"){
			messageresponse = "- Mamá, en el colegio me llaman distraído, - Juanito, tu vives en la casa de enfrente :D :D";
		}		
		
        // Crear un payload para un simple mensaje de texto
        var response = {
            "text": '' + messageresponse
        }
    }
    
    // Enviamos el mensaje mediante SendAPI
    enviar_texto(senderID, response);
}

// Funcion donde el chat respondera usando SendAPI
function enviar_texto(senderID, response){
    // Construcicon del cuerpo del mensaje
    let request_body = {
        "recipient": {
          "id": senderID
        },
        "message": response
    }
    
    // Enviar el requisito HTTP a la plataforma de messenger
    request({
        "uri": "https://graph.facebook.com/v2.6/me/messages",
        "qs": { "access_token": process.env.PAGE_ACCESS_TOKEN },
        "method": "POST",
        "json": request_body
    }, (err, res, body) => {
        if (!err) {
          console.log('Mensaje enviado!')
        } else {
          console.error("No se puedo enviar el mensaje:" + err);
        }
    }); 
}